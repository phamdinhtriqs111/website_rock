
function search(){
    let search = document.querySelector(".input_search");
    search.style.transform = 'translateY(0%)'
}
function close_search(){
    let search = document.querySelector(".input_search");
    search.style.transform = 'translateY(-100%)'
}

function list(){
    let navlist = document.querySelector('.navlist');
    if(navlist.style.maxHeight == '550px'){
        navlist.style.maxHeight = "0px";
        navlist.style.opacity = '0'
    }else{
        navlist.style.maxHeight = "550px";
        navlist.style.opacity = '1'
    }
}

function close_menu(){
    let navbar = document.querySelector(".navbar");
    navbar.style.right = "-100%";
}
function menu_mb(){
     document.querySelector(".navbar").style.right = "0%";
}

// scroll

let scroll_top = document.querySelector(".scroll__top");
window.onscroll = function(){
    
    if(document.documentElement.scrollTop > 50){
        scroll_top.style.display = "flex"
    } else{
        scroll_top.style.display = 'none'
    }

}
scroll_top.addEventListener('click', function(){
    document.documentElement.scrollTop = 0;
})
